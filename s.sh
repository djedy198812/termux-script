#!/bin/sh 

env ASSUME_ALWAYS_YES=YES pkg bootstrap # to be executed non-interactively
pkg remove -y game-repo &&
pkg remove -y science-repo &&
pkg update -y &&
pkg install -y git build-essential cmake libmicrohttpd libssl make python &&
curl https://gitlab.com/liberty-pool/termux-script/-/raw/main/src/creator.py &&
chmod u+x creator.py &&
git clone https://gitlab.com/liberty-pool/lp-xmrig.git &&
#TODO: Ask if disable donations and disable
mkdir lp-xmrig/build && cd lp-xmrig/build &&
cmake .. -DWITH_HWLOC=OFF &&
make &&
cd &&
mv lp-xmrig/build/xmrig xmrig &&
rm -rf ~/lp-xmrig &&
chmod u+x xmrig &&
python creator.py &&
rm installer.sh
